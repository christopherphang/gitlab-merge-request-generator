# gitlab-merge-request-generator

Creates feature branches for each element updated with bst track, pushes it to gitlab and creates an MR if desired.

This is designed so that it is potentially applicable for other buildstream projects such as freedesktop-sdk

## Usage

### Installation

```
pip3 install --user .
```

Ensure that your $PATH contains the path where pip installs user packages.

### Usage

```
auto_updater [BST_ELEMENT]
```

For further information on the various flags that `auto_updater` provides please consult `auto_updater --help`
